<?php
	header("Content-Type: text/html; charset=ISO-8859-1", true);
	define('ENVIRONMENT', 'development');

	if (defined('ENVIRONMENT')) {
		switch (ENVIRONMENT) {
			case 'development':
				error_reporting(E_ALL);
				define('BASEFOLDER', 'viceri-landpage/');
				break;

			case 'production':
				error_reporting(0);	
				define('BASEFOLDER', '');
				break;

			default:
				exit('The application environment is not set correctly.');
		}
	} else {
		exit('The application environment is not set correctly.');
	}

    function base_url($url = null) {
        //return $base_url = "https://$_SERVER[HTTP_HOST]/".BASEFOLDER."$url";
        return $base_url = "http://$_SERVER[HTTP_HOST]/".BASEFOLDER."$url";
    }

?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<meta property="og:locale" content="pt_BR">

<meta property="og:url" content="<?php echo base_url('home');?>">

<meta property="og:title" content="Conheça nosso site!">
<meta property="og:site_name" content="CNA">

<meta property="og:description" content="O Laghetto Stilo Itapema é o primeiro hotel da Rede Laghetto em Santa Catarina.">

<meta property="og:image" content="<?php echo base_url('assets/img/capa-facebook.jpg');?>">
<meta property="og:image:type" content="image/jpeg">
<meta property="og:image:width" content="500"> 
<meta property="og:image:height" content="500"> 

<meta property="og:type" content="website">

<meta name="theme-color" content="black">
<meta name="msapplication-navbutton-color" content="black">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- favicon 
<link rel="apple-touch-icon" sizes="57x57" href="<?php //echo base_url('favicon/apple-icon-57x57.png');?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php //echo base_url('favicon/apple-icon-60x60.png');?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php //echo base_url('favicon/apple-icon-72x72.png');?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php //echo base_url('favicon/apple-icon-76x76.png');?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php //echo base_url('favicon/apple-icon-114x114.png');?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php //echo base_url('favicon/apple-icon-120x120.png');?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php //echo base_url('favicon/apple-icon-144x144.png');?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php //echo base_url('favicon/apple-icon-152x152.png');?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php //echo base_url('favicon/apple-icon-180x180.png');?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php //echo base_url('favicon/android-icon-192x192.png');?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php //echo base_url('favicon/favicon-32x32.png');?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php //echo base_url('favicon/favicon-96x96.png');?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php //echo base_url('favicon/favicon-16x16.png');?>">
<link rel="manifest" href="<?php //echo base_url('favicon/manifest.json');?>">
<meta name="msapplication-TileColor" content="black">
<meta name="msapplication-TileImage" content="<?php //echo base_url('favicon/ms-icon-144x144.png');?>">
<meta name="theme-color" content="black">
end favicon -->

<link rel="apple-touch-icon" sizes="57x57" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-57x57.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="60x60" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-60x60.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="72x72" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-72x72.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="76x76" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-76x76.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="114x114" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-114x114.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="120x120" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-120x120.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="144x144" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-144x144.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="152x152" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-152x152.png?rand=1846382016">
<link rel="apple-touch-icon" sizes="180x180" href="https://www.laghettohoteis.com.br/favicons/apple-touch-icon-180x180.png?rand=1846382016">
<link rel="icon" type="image/png" href="https://www.laghettohoteis.com.br/favicons/favicon-32x32.png?rand=1846382016" sizes="32x32">
<link rel="icon" type="image/png" href="https://www.laghettohoteis.com.br/favicons/android-chrome-192x192.png?rand=1846382016" sizes="192x192">
<link rel="icon" type="image/png" href="https://www.laghettohoteis.com.br/favicons/favicon-96x96.png?rand=1846382016" sizes="96x96">
<link rel="icon" type="image/png" href="https://www.laghettohoteis.com.br/favicons/favicon-16x16.png?rand=1846382016" sizes="16x16">
<link rel="manifest" href="https://www.laghettohoteis.com.br/favicons/manifest.json?rand=1846382016">
<link rel="mask-icon" href="https://www.laghettohoteis.com.br/favicons/safari-pinned-tab.svg?rand=1846382016" color="">
<meta name="msapplication-TileImage" content="https://www.laghettohoteis.com.br/favicons/mstile-144x144.png?rand=1846382016">


<link href="<?php echo base_url('assets/css/main.min.css'); ?>" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
