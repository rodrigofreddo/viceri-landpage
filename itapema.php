﻿<!doctype html>
<html lang="pt-br">

<head>
    <title>Laguetto Stilo Itapema</title>

    <meta name="description" content="O Laghetto Stilo Itapema &eacute; o primeiro hotel da Rede Laghetto em Santa Catarina." />
    <?php include('includes/head.php') ?>

</head>

<body class="page-itapema">

    <section id="header">

        <div class="container">

            <div class="row row justify-content-center">

                <div class="col-lg-2 col-md-2 col-sm-4 col-4">

                    <a href="<?php echo base_url('home'); ?>">
                        <h1><img class="logo-header" title="Hotel Laghetto Stilo" alt="Hotel Laghetto Stilo" src="<?php echo base_url('assets/img/logo-header.png'); ?>"></h1>
                    </a>

                </div>

                <div class="col-lg-8 col-md-10 d-md-block d-none text-right">

                    <p class="address white font-12 regular d-block">(47) 3514-4900 | (47) 3368-1792 - Meia Praia - Itapema - SC</p>

                    <nav class="menus-header">

                        <ul>

                            <li>

                                <a href="<?php echo base_url('home'); ?>" class="font-16 bold white" title="Infraestrutura">O Hotel</a>

                            </li>

                            <li>

                                <a href="<?php echo base_url('home'); ?>" class="font-16 bold white" title="Infraestrutura">Infraestrutura</a>

                            </li>

                            <li>

                                <a href="<?php echo base_url('home'); ?>" class="font-16 bold white" title="Apartamentos">Apartamentos</a>

                            </li>

                            <li>

                                <a href="<?php echo base_url('home'); ?>" class="font-16 bold white" title="Restaurante">Restaurante</a>

                            </li>

                             <li>

                                <a href="<?php echo base_url('itapema'); ?>" class="font-16 bold white active" title="Itapema">Itapema</a>

                            </li>

                            <!--<li>
                                    
                                <a href="<?php //echo base_url('home'); ?>" class="font-16 bold white" title="Negócios CNA">Negócios CNA</a>

                            </li>-->

                            <li>

                                <a href="https://myreservations.omnibees.com/chain.aspx?c=2143&ad=1&CheckIn=&CheckOut=&hotelname=&Code=&sid=cc758d49-fb2b-44a3-80a4-be57a0650f0a&version=MyReservation" target="_blank" class="font-16 bold white btn-reserva" title="Reservas">Reservas</a>

                            </li>

                        </ul>

                    </nav>

                </div>

                <div class="col-8 d-md-none d-block text-right">

                    <div class="menu-mobile-btn" onClick="javascript:chamaMenu();">
                        <svg id="menu-svg-morph" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="width:30px;fill:white;">
                            <path class="hamburger" id="hamburger" d="M1.71,19.2H22.29c.94,0,1.71,1.08,1.71,2.4S23.23,24,22.29,24H1.71C.77,24,0,22.92,0,21.6S.77,19.2,1.71,19.2Zm20.58-4.8H1.71C.77,14.4,0,13.32,0,12S.77,9.6,1.71,9.6H22.29C23.23,9.6,24,10.68,24,12S23.23,14.4,22.29,14.4Zm0-9.6H1.71C.77,4.8,0,3.72,0,2.4S.77,0,1.71,0H22.29C23.23,0,24,1.08,24,2.4S23.23,4.8,22.29,4.8Z" transform="translate(0 0)" />
                            <path style="visibility: hidden;" class="xis" id="xis" d="M23.73,2.88a.92.92,0,0,1,0,1.3l-7.17,7.17a1,1,0,0,0,0,1.31l7.17,7.17a.92.92,0,0,1,0,1.3l-2.61,2.6a.93.93,0,0,1-1.31,0l-7.16-7.17a.92.92,0,0,0-1.3,0L4.18,23.73a.93.93,0,0,1-1.31,0L.27,21.12a.92.92,0,0,1,0-1.3l7.17-7.17a.92.92,0,0,0,0-1.3L.27,4.18a1,1,0,0,1,0-1.31L2.88.27a.93.93,0,0,1,1.31,0l7.16,7.17a.92.92,0,0,0,1.3,0L19.82.27a1,1,0,0,1,1.31,0Z" transform="translate(0 0)" />
                            <path style="visibility: hidden;" class="arrow" id="arrow" d="M22.29,10.24H5.83l4-4.62a2.08,2.08,0,0,0,.5-1.38,1.9,1.9,0,0,0-1.72-2,1.5,1.5,0,0,0-1.18.58l-6.82,8A2.09,2.09,0,0,0,0,12.24a2,2,0,0,0,.55,1.44l6.84,8a1.54,1.54,0,0,0,1.18.58,1.91,1.91,0,0,0,1.72-2,2,2,0,0,0-.5-1.38l-4-4.62H22.29a1.88,1.88,0,0,0,1.71-2A1.87,1.87,0,0,0,22.29,10.24Z" transform="translate(0 0)" />
                        </svg>
                    </div>

                    <div class="wrapper-menu-lista">
                        <!-- paginas -->
                        <ul>

                            <li>
                                <a href="<?php echo base_url('home'); ?> chamaMenu();">Infraestrutura</a>
                            </li>

                            <li>
                                <a href="<?php echo base_url('home'); ?> chamaMenu();">Apartamentos</a>
                            </li>

                            <li>
                                <a href="<?php echo base_url('home'); ?> chamaMenu();">Restaurante</a>
                            </li>

                            <li>
                                <a href="<?php echo base_url('itapema'); ?>" onclick="chamaMenu();">Itapema</a>
                            </li>

                            <!--<li>
                                <a href="<?php //echo base_url('home'); ?>" onclick="chamaMenu();">Negócios CNA</a>
                            </li>-->

                            <li>
                                <a title="Reservas" target="_blank" href="https://myreservations.omnibees.com/chain.aspx?c=2143&ad=1&CheckIn=&CheckOut=&hotelname=&Code=&sid=cc758d49-fb2b-44a3-80a4-be57a0650f0a&version=MyReservation">
                                    <p>Reservas</p>
                                </a>
                            </li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <main class="main">
    <section id="banners-home">

        <div class="wrapper-banner">

            <div class="banner banner-1">

                <div class="container">

                    <div class="row">

                        <div class="col-12 text-center">

                            <h1 class="font-50 bold white title-principal">Venha para Itapema / SC</h1>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section id="itapema">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start">
                <div class="col-lg-1 d-none d-lg-block"></div>
                <div class="col-sm-10 col-lg-5">
                    <h1 class="title">Itapema</h1>
                    <p>Itapema é a cidade com a melhor infraestrutura entre as praias no Litoral Norte de Santa Catarina e considerada a cidade que mais cresce no Estado.</p>
                    <p>Meia Praia é o coração de Itapema. É a maior e mais famosa praia da cidade e a que possui melhor infraestrutura turística: bares, restaurantes, casas noturnas, comércio e serviços em toda a orla e na avenida principal.</p>
                    <p>Quem gosta de movimento também pode ir para a Praia do Centro: é lá que são realizados inúmeros eventos esportivos. Já o Canto da Praia é para quem gosta de sossego, um lugar calmo e reduto dos pescadores. Aos amantes da natureza, a Praia Grossa, cercada de mata nativa preservada, é o destino certo. Para os surfistas, melhor ir para a Praia do Estaleiro e a Praia da Ilhota, que são agrestes, de mar aberto e com ondas fortes.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="atividades">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start">
                <div class="col-lg-1 d-none d-lg-block"></div>
                <div class="col-sm-10 col-lg-9 col-xl-7">
                    <h1 class="title">O que fazer na região</h1>
                    <div class="atividade">
                        <img width="260" height="202" class="img" src="<?php echo base_url('assets/img/itapema/beto_carrero.jpg'); ?>">
                        <h2 class="subtitle">BETO CARRERO</h2>
                        <p class="last">Localizado em Penha/SC, a poucos quilômetros e Itapema o Beto Carrero é o maior parque temático da América Latina! Mais de 100 incríveis atrações para toda a família! Brinquedos radicais e familiares, 7 shows ao vivo diariamente e o mais belo zoológico do País!
                        </p>
                        <img width="260" height="202" class="img2" src="<?php echo base_url('assets/img/itapema/beto_carrero.jpg'); ?>">
                        <a class="link-atividade" target="_blank" title="Consulte horário de funcionamento" href="https://www.betocarrero.com.br/calendario-de-funcionamento">Mais informações</a>
                    </div>
                    <div class="atividade">
                        <img width="260" height="202" class="img" src="<?php echo base_url('assets/img/itapema/santuario.jpg'); ?>">
                        <h2 class="subtitle">SANTUÁRIO SANTA PAULINA</h2>
                        <p>A partir do dia 18 de outubro de 1991, após a beatificação de Madre Paulina, pelo Papa Joao Paulo II, por ocasião da sua segunda visita ao Brasil, peregrinos de todo o país começaram a visitar o bairro de Vígolo em Nova Trento/SC, terra onde Santa Paulina começou a obra da Imaculada.</p>
                        <p class="last">Em 09 de maio de 2002 – Madre Paulina é Canonizada, na Praça de São Pedro, e passa a ser chamada de Santa Paulina do Coração Agonizante de Jesus.</p>
                        <img width="260" height="202" class="img2" src="<?php echo base_url('assets/img/itapema/santuario.jpg'); ?>">
                        <a class="link-atividade" title="Santuário Santa Paulina" target="_blank" href="https://santuariosantapaulina.org.br/">Mais informações</a>
                    </div>
                    <div class="atividade">
                        <img width="260" height="202" class="img" src="<?php echo base_url('assets/img/itapema/porto_belo.jpg'); ?>">
                        <h2 class="subtitle">ILHA DE PORTO BELO</h2>
                        <p>A aproximadamente 5 quilômetros de Itapema a Ilha de Porto Belo encanta pela beleza paradisíaca. Sua natureza preservada é um atrativo especial, em meio à comodidade da infra-estrutura oferecida ao visitante.</p>
                        <p>O uso turístico-recreativo respeita a capacidade máxima de visitantes, avaliada em 1.879 pessoas por dia, de acordo com estudo de profissionais especializados em turismo sustentável.
                        </p>
                        <p>Na alta temporada, o atendimento é feito também por estagiários do curso Turismo e Hotelaria, através do projeto Gentis Orientadores. O trabalho conjunto com as embarcações dos pescadores artesanais e as escunas garante um passeio seguro e divertido, cheio de conhecimento e aventuras.
                        </p>
                        <p>Embarcados nestes pitorescos barcos de pescadores ou nas escunas com animação a bordo, os turistas desfrutam de um passeio agradável e lindas paisagens, além de conhecer um pouco da história da baía até chegar à Ilha de Porto Belo.</p>
                        <p class="last">O atendimento ao público acontece das 09h às 18h na alta temporada  (1º de dezembro a 31 de março). Nos outros meses do ano, a ilha fica aberta mas os serviços ficam suspensos. Para grupos, favor entrar em contato e agendar a visita.
                        </p>
                        <img width="260" height="202" class="img2" src="<?php echo base_url('assets/img/itapema/porto_belo.jpg'); ?>">
                        <a class="link-atividade" title="Ilha de Porto Belo" target="_blank" href="http://www.ilhadeportobelo.com.br/">Mais informações</a>
                    </div>
                    <div class="atividade">
                        <img width="260" height="202" class="img" src="<?php echo base_url('assets/img/itapema/oktoberfest.jpg'); ?>">
                        <h2 class="subtitle">OKTOBERFEST DE BLUMENAU/SC </h2>
                        <p>Blumenau, colonizada por alemães, sempre manteve viva a cultura e as tradições de seus antepassados. No início da década de 80, empresários e representantes de entidades de classe e o poder público da cidade se uniram e idealizaram um evento onde esta cultura pudesse ser celebrada e mostrada ao mundo. Inspirada naturalmente pela homônima alemã nasceu então, em 1984, a Oktoberfest Blumenau.  Naquele primeiro ano, em apenas 10 dias de festa, mais de 100mil pessoas visitaram o evento.</p>
                        <p>Na década de 90 e início dos anos 2000 a festa consolidou-se como um dos maiores eventos turísticos do Brasil e uma das maiores Oktoberfests do mundo. A tradição nunca foi deixada de lado, mas nessa época a protagonista do evento era a cerveja.
                        </p>
                        <p class="last">A cada ano, durante alguns dias do mês de outubro mais de 500 mil pessoas visitam a Oktoberfest Blumenau e experenciam o amor pelas tradições, música, dança, trajes típicos e gastronomia típica. Tudo isso com muita alegria  e, claro, um bom chope. Prost!
                        </p>
                        <img width="260" height="202" class="img2" src="<?php echo base_url('assets/img/itapema/oktoberfest.jpg'); ?>">
                        <a class="link-atividade" title="Oktoberfest de Blumenau/SC" target="_blank" href="http://oktoberfestblumenau.com.br/">Mais informações</a>

                    </div>
                    <div class="atividade">
                        <img width="260" height="202" class="img" src="<?php echo base_url('assets/img/itapema/gideoes.jpg'); ?>">
                        <h2 class="subtitle">GIDEÕES MISSIONÁRIOS DA ULTIMA HORA</h2>
                        <p>Gideões Missionários da Última Hora, com sede em Camboriú, SC, tem por objetivo principal a divulgação do Evangelho de Cristo no Brasil e no mundo.</p>
                        <p class="last">Prepara e envia missionários em busca das almas. Mais de mil famílias missionárias sendo sustentadas no campo missionário, o que estima-se em mais de três mil pessoas trabalhando em prol do Reino de Deus em 43 nações, totalizando dezenas de Projetos Missionários.
                            A cada mês de abril acontece em Camboriú o maior evento missionário do mundo, que tem como única visão conscientizar e unir o Brasil para evangelizar o mundo. No evento de 2019 esteve presente o Presidente da República Jair Bolsonaro.
                        </p>
                        <img width="260" height="202" class="img2" src="<?php echo base_url('assets/img/itapema/gideoes.jpg'); ?>">
                        <a class="link-atividade" title="Gideões Missionários da Última Hora" target="_blank" href="https://www.gideoes.com.br/">Mais informações</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aeroportos">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start">
                <div class="col-1 d-none d-md-block"></div>
                <div class="col-sm-10 col-md-10 col-lg-6 col-xl-5">
                    <h1 class="title">Aeroportos</h1>
                    <a href="https://floripa-airport.com/" target='_blank'><strong>Aeroporto Internacional De Florianópolis</strong></a>
                    <p>
                        O Aeroporto Internacional Hercílio Luz,  fica localizado no Sul da Ilha, distante cerca de 12km do Centro de Florianopolis, na Av. Dep. Diomício Freitas, 3393, há apenas 80km de Itapema.
                    </p>
                        
                    <a href="http://www4.infraero.gov.br/aeroportos/aeroporto-internacional-de-navegantes-ministro-victor-konder/" target='_blank'><strong>AEROPORTO INTERNACIONAL DE NAVEGANTES/SC., MINISTRO VICTOR KONDER</strong></a>
                        
                    <p>
                        O aeroporto Internacional Minístro Victor Konder, fica localizado na rua Osmar Gaya, 1.297, Navegantes/sc., distante aproximadamente 40km de Itapema.
                    </p>

                    <a href="http://www.grupocostaesmeralda.com.br/" target='_blank'><strong>Condominio Aeronautico Costa Esmeralda </strong></a>
                        
                    <p>
                        Localizado na BR-101, KM 156 - Santa Luzia Porto Belo/SC., distante 7 Km de Itapema, o Condomínio Aeronáutico Costa Esmeralda conta com uma pista oficial de 1.190 mts por 23 mts, distante 500 metros da BR 101, contando com abastecimento com AVGÁS e JetA1.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="distancia">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start">
                <div class="col-1 d-none d-md-block"></div>
                <div class="col-sm-10 col-md-10 col-lg-6 col-xl-5">
                    <h1 class="title">Distâncias</h1>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Destino</th>
                                <th>Distância</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Balneário Camboriú</td><td>(13 km) via BR-101</td>
                            </tr>
                            <tr>
                                <td>Penha (Beto Carrero World)</td><td>(48,7 km) via BR-101</td>
                            </tr>
                            <tr>
                                <td>Porto Belo</td><td>17 km</td>
                            </tr>
                            <tr>
                                <td>Governador Celso Ramos</td><td>47 km</td>
                            </tr>
                            <tr>
                                <td>Nova Trento (Santuário de Santa Paulina)</td><td>52 km</td>
                            </tr>
                            <tr>
                                <td>Curitiba</td><td>234 km</td>
                            </tr>
                            <tr>
                                <td>São Paulo</td><td>(692,7 km) via BR-101 e BR-116</td>
                            </tr>
                            <tr>
                                <td>Itajaí</td><td>(31 km) via BR-101</td>
                            </tr>
                            <tr>
                                <td>Brusque</td><td>54 km</td>
                            </tr>
                            <tr>
                                <td>Bombinhas</td><td>22 km</td>
                            </tr>
                            <tr>
                                <td>Navegantes</td><td>45 km</td>
                            </tr>
                            <tr>
                                <td>Florianópolis</td><td>66 km</td>
                            </tr>
                            <tr>
                                <td>Porto Alegre</td><td>506,7 km</td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </section>
    </main>
    <section id="localizacao">

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3550.6079472874526!2d-48.5996961852876!3d-27.137155909137583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94d8afccdd597bfd%3A0x18f525ce3e447cf3!2sHotel+Laghetto+Stilo+Itapema!5e0!3m2!1spt-BR!2sbr!4v1558306821215!5m2!1spt-BR!2sbr" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

    </section>

    <section id="reservas">

        <div class="container">

            <div class="row justify-content-center text-xl-left text-center">

                <div class="col-xl-2"></div>

                <div class="col-xl-9 col-lg-11">

                    <p class="font-22 bold white text-md-left text-center">Cadastre-se e receba<br> nossas novidades:</p>

                    <form id="form-contato" method="post">

                        <input type="text" name="Enome" id="Enome" placeholder="Nome:" class="required">

                        <input type="text" name="Eemail" id="Eemail" placeholder="E-mail:" class="required" onKeyPress="javascript:if (event.keyCode == 13){ APP.enviarContato(event); }">

                        <button title="Enviar" type="button" id="btn-contato" onclick="javascript: APP.enviarContato(event);" class="btn-enviar bold font-16 white">Enviar</button>

                    </form>

                </div>

                <div class="col-xl-1"></div>

            </div>

        </div>

    </section>

    <!--<section id="footer">

        <div class="container">

            <div class="row">

                <div class="col-12 text-center">

                    <img title="Hotel Laghetto Stilo" alt="Hotel Laghetto Stilo" src="<?php //echo base_url('assets/img/logo-footer.png'); ?>">

                    <p class="font-12 regular black">(47) 3368-1792 | (47) 3514-4276</p>
                    <p class="font-12 regular black">Rua 292, nº 139 - Meia Praia - Itapema - SC</p>

                </div>

            </div>

        </div>

    </section>-->

    <section id="footer">
            
            <div class="container">
                
                <div class="row justify-content-center">
                    
                    <div class="col-lg-10 text-sm-left text-center">
                        
                        <img title="Hotel Laghetto Stilo" alt="Hotel Laghetto Stilo" src="<?php echo base_url('assets/img/logo-header.png'); ?>">   

                        <div class="info-footer">
                            <p class="font-12 regular white">(47) 3514-4900 | (47) 3368-1792</p>
                            <p class="font-12 regular white">Rua 292, nº 139 - Meia Praia - Itapema - SC</p>
                            <a target="_blank" href="https://www.cnaconstrutora.com.br/" title="CNA Construtora"><img src="<?php echo base_url('assets/img/marca-rodape.png'); ?>"></a>
                        </div>

                    </div>

                </div>

            </div>

        </section>

    <div class="modal" id="modalCamposObrigatorios">
        <div class="modal-dialog">
            <div class="modal-content modal-contato">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>-->
                    <div class="font-25 bold black">Erro Preenchimento de formulário!</div>
                </div>
                <div style="margin: auto;" class="modal-body font-16 regular black">
                    Favor preencher todos os campos obrigatórios.
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin: auto; cursor: pointer;" onclick="javascript:APP.hideModal('modalCamposObrigatorios')" class="btn-reservar bold white" data-dismiss="modal">Fechar</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div><!-- AVISO DE E-MAIL INVALIDO -->

    <div class="modal" id="modalOk">
        <div class="modal-dialog">
            <div class="modal-content modal-contato">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>-->
                    <div class="font-25 bold black">Preenchimento de formulário!</div>
                </div>
                <div style="margin: auto;" class="modal-body font-16 regular black">
                    Obrigado por se inscrever! Nossas novidades chegarão no seu e-mail!
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin: auto; cursor: pointer;" onclick="javascript:APP.hideModal('modalOk')" class="btn-reservar bold white" data-dismiss="modal">Fechar</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div><!-- AVISO DE E-MAIL INVALIDO -->

    <div class="modal" id="modalErroInterno">
        <div class="modal-dialog">
            <div class="modal-content modal-contato">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>-->
                    <div class="font-25 bold black">Erro Preenchimento de formulário!</div>
                </div>
                <div style="margin: auto;" class="modal-body font-16 regular black">
                    Ocorreu um erro interno! Tente novamente mais tarde.
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin: auto; cursor: pointer;" onclick="javascript:APP.hideModal('modalErroInterno')" class="btn-reservar bold white" data-dismiss="modal">Fechar</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div><!-- AVISO DE E-MAIL INVALIDO -->

    <div class="modal" id="modalEmailInvalido">
        <div class="modal-dialog">
            <div class="modal-content modal-contato">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>-->
                    <div class="font-25 bold black">Erro Preenchimento de formulário!</div>
                </div>
                <div style="margin: auto;" class="modal-body font-16 regular black">
                    O e-mail informado não é válido!
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin: auto; cursor: pointer;" class="btn-reservar bold white" onclick="javascript:APP.hideModal('modalEmailInvalido')" data-dismiss="modal">Fechar</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div><!-- AVISO DE E-MAIL INVALIDO -->

    <script type="text/javascript" src="<?php echo base_url('assets/js/vendor.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js?v=1'); ?>" charset="utf-8"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">
        function scrollToTarget($target) {
            $('html,body').animate({
                scrollTop: $($target).offset().top
            }, 'slow');
        }
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140687539-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-140687539-1');
    </script>

</body>

</html>