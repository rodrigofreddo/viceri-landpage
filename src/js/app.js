var root_path = window.location.protocol + '//' + document.location.hostname + '/viceri-landpage';
//var root_path = window.location.protocol + '//' + document.location.hostname;

var APP = {
	init: function () {
		this.setup();
		this.start();
		this.bind();
	},

	setup: function() {
		this.url = base_url;
		this.menuHeader = $('.menu-header');
		this.menuMobileBtn = $('.menu-mobile-btn');
		this.submenuMobileLink = $('.wrapper-menu-list a');
		this.menuMobile = $('.menu-mobile');
		this.inputPhone = $('.mask-input.phone');
		this.banner = $('#banner-home');
		this.imagesNews = $('#slide-news');
		this.carouselBus = $('#fleet #carousel');
		this.carouselBusItem = $('#fleet .details .item');
		this.btnBuy = $('.travels-details #btn-buy');
		this.btnTestimonial = $('#testimonials #btn-testimonial');
		this.contactForm = $('#contact-form');
		this.contactFormBtn = $('#contact-form a.btn');
		this.btnNewsletter = $('.btn-newsletter');
	},

	start: function() {
		console.log('APP started.');

		this.startBanner();
		this.maskForms();
		this.slideNews();
		this.startMenuMobile();
		this.galleryHome();
		this.formMask();
	},

	formMask: function(){
    	$(".mask-phone").mask("(99) 99999-999#").focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-9999");  
            } else {  
                element.mask("(99) 9999-9999#");  
            }  
        });
    	$('.mask-date').mask('00/00/0000');
    	$('.mask-value').mask('000.000.000,00',{reverse: true});
    },

    scrollToTarget: function($target) {
		$('html,body').animate({
		scrollTop: $($target).offset().top},'slow');
	},

    enviaForm: function(event) {
    	event.preventDefault();
        var $targetButton = $('#btn-curriculo');
        var id = $targetButton.data('form');
        var $form = $('#'+id);

        if (validaForm(id)) {
            // Desabilita o botão e começa o envio
            $targetButton.attr('disabled', 'disabled');
            $targetButton.text('Enviando...');
            $targetButton.css('cursor', 'progress');

            $.ajax({
                url: 'contato',
                method: 'POST',
                data: new FormData($('#'+id).get(0)),
                complete: function ()  { 
                    // Reabilita o botão
                    console.log("complete");
                    $targetButton.removeAttr('disabled');
                    $targetButton.text('Enviar Currículo');
                    $targetButton.css('cursor', 'pointer');
                },
                success: function (data)  {
                  		console.log('sucesso');

                  	var msg = "";


		        	if (data == 'ok') {
		        		msg = "Cadastrado com sucesso";
		        		func = function() {
		        			$form.get(0).reset();
		        		};
		        	} else if (data == 'problema-arquivo') {
						msg = "Problema com o arquivo\n Cheque se o tipo era 'pdf'";
		        	} else if (data == 'problema') {
						msg = "Problema ao realizar cadastro\n Por favor, tente novamente mais tarde";
		        	}



		        	Sweetalert2(msg).then(function () {
						func();
					});
                   
                },
                cache: false,
		        contentType: false,
		        processData: false,

                error: function (data)  {
                    // eslint-disable-next-line no-console
                    console.log(data);
                }
            });
        } else {
            console.log('notok');
            Sweetalert2("Campos obrigatórios não preenchidos!");
        }
    },

    modalAvisosGerais: function(options) {
        var $modalAvisos = $('#modal-avisos-gerais');
        var $title = $modalAvisos.find('.title');
        var $texto = $modalAvisos.find('.texto');

        $title.html(options.title || '');
        $texto.html(options.texto || '');

        $modalAvisos.modal();
    },

	validaForm: function(form){
		var _this = this;

		var required = form.find('.required');
		var select = form.find('select.required');
		var requiredSelect = form.find('select.required');
		var aceite = form.find('.aceite');
		var requiredAceite = form.find('.aceite .required:checked');
		var req = 0;
		var reqSel = 0;
		var reqAc = 0;
		var classe;

		for(i=0;i<required.length;i++){
			if($(required[i]).val() == "" && $(required[i]).prop('disabled') != true){
				classe = $(required[i]).attr('class');
				
				if(classe.search("required") != -1){	
					$(required[i]).addClass('error');
				}
				req = 1;
			}else{
				classe = $(required[i]).attr('class');
				
				if(classe.search("error") != -1){
					$(required[i]).removeClass('error');
				}
			}
		}

		if(select.length > 0){
			for(i=0;i<requiredSelect.length;i++){
				if($(requiredSelect[i]).find(":selected").text() == "Selecione"){
					classe = $(requiredSelect[i]).attr('class');
					
					if(classe.search("required") != -1){	
						$(requiredSelect[i]).addClass('error');
					}
					reqSel = 1;
				}else{
					classe = $(requiredSelect[i]).attr('class');
					
					if(classe.search("error") != -1){
						$(requiredSelect[i]).removeClass('error');
					}
				}
			}
		}

		if(aceite.length > 0){
			_this.projetoAceite.removeClass('error');
			if(requiredAceite.length == 0){
				_this.projetoAceite.addClass('error');
				reqAc = 1;
			}else{
				_this.projetoAceite.removeClass('error');
			}
		}
		
		if(req == 1 || reqSel == 1 || reqAc == 1){
			_this.goToElement($('.error').first());
			return false;
		}else if(req == 0 && reqSel == 0 && reqAc == 0){
			return true;
		}
	},

	maskForms: function() {
        var _this = this;

        this.inputPhone.mask("(99) 99999-999#").focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-9999");  
            } else {  
                element.mask("(99) 9999-9999#");  
            }  
        });
	},

	startMenuMobile: function() {
        var _this = this;

        this.menuMobile.css('height',$(window).height());
	},

	galleryHome: function() {
		$('.fancybox').fancybox({
			thumbs : {
				autoStart : true
			}
		});
	},

	slideNews: function() {
        var _this = this;

        this.imagesNews.slick({
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: false,
            responsive: [{
				breakpoint: 960,
				settings: {
					adaptiveHeight: true
				}
			}]
        });
    },

	startBanner: function() {
        var _this = this;

        this.banner.slick({
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 6000,
            pauseOnHover: true,
            responsive: [{
				breakpoint: 960,
				settings: {
					adaptiveHeight: true
				}
			}]
        });

        this.banner.on('beforeChange', function(event, slick, currentSlide, nextSlide){
			$('.background-pacotes').removeClass('active');
			$('[data-pacote="'+nextSlide+'"]').addClass('active');
		});
    },

    openMenu: function(){
		if(!$('body').hasClass('sub-menu-aberto-body')){
			$('body').toggleClass('menu-open');

			if( $('body').hasClass('menu-open') ){
				TweenMax.to(".hamburger", 0.4, {morphSVG:'#xis'});
			}else{
				TweenMax.to(".hamburger", 0.4, {morphSVG:'#hamburger'});
			}

		}else{
			$('.sub-menu-aberto-body').removeClass('sub-menu-aberto-body');
			$('.sub-menu-aberto').removeClass('sub-menu-aberto');
			TweenMax.to(".hamburger", 0.4, {morphSVG:'#xis'});
		}
	},

    openSubmenu: function(submenu){
		$('[data-menu="'+submenu+'"]').toggleClass('sub-menu-aberto');
		$('body').toggleClass('sub-menu-aberto-body');

		if( $('body').hasClass('sub-menu-aberto-body') ) {
			TweenMax.to(".hamburger", 0.4, {morphSVG:'#arrow'});
		}
	},

	goToElement: function(el) {
        $("html, body").animate({
            scrollTop: (($(window).width() > 960) ? $(el).offset().top - 50 : $(el).offset().top - 40)
        }, 600);
    },

	bind: function() {
		var _this = this;

		this.menuMobileBtn.on('click', function() {
			_this.openMenu();
		});

		this.submenuMobileLink.on('click', function() {
			if($(this).attr('data-submenu') != ''){
				_this.openSubmenu($(this).attr('data-submenu'));
			}
		});

		this.contactFormBtn.on('click', function() {
			_this.sendContact();
		});

		this.btnNewsletter.on('click', function() {
			 
			swal({
				title: 'Inscreva-se',
			  	confirmButtonText: 'ENVIAR',
			  	animation: false,
				html:
					'<input type="text" id="Ename" class="swal2-input" placeholder="Nome">' +
					'<input type="text" id="Eemail" class="swal2-input" placeholder="Email">' +
					'<input type="hidden" id="Etoken" class="swal2-input" value="'+$('.promotion.newsletter input[name=_token]').val()+'">',
				preConfirm: function () {
					return new Promise(function (resolve) {
						resolve([
							$('#Ename').val(),
							$('#Eemail').val(),
							$('#Etoken').val()
						])
					})
				},
				onOpen: function () {
					$('#Ename').focus()
				}
			}).then(function (result) {
			 	
				var dados = { Ename: result.value[0], Eemail: result.value[1], _token: result.value[2] };

				if(result.value[0] != '' && result.value[1] != '' && result.value[3] != ''){
					$.ajax({
						type: "POST",
						url: _this.url+"/newsletter",
		    			data: dados,
						success: function(data) {
							if(data.trim() == 'success'){
								swal(
									'Sua inscrição foi realizada. Obrigado.'
								);
							}else{
								swal(
									'Houve um erro ao enviar sua inscrição, tente novamente mais tarde. ERRO 02.'
								);
							}
						}, 
						error: function(data) {
							swal(
								'Houve um erro ao enviar sua inscrição, tente novamente mais tarde. ERRO 03.'
							);
						}
					});
				}else{
					swal(
						'Preencha os campos corretamente!'
					);
				}

			}).catch(swal.noop)

		});

	}
};

$(document).ready(function() {
	APP.init();
});