<?php session_start();

header("Content-Type: text/html; charset=ISO-8859-1", true);

require_once ('../model/News.php');
require_once ('../dao/NewsDAO.php');

$status = 'ok';
$dao = null;
try{

    $obj = new News();
    $obj->email = utf8_decode($_POST["email"]);

    $dao = new NewsDAO(null);
    $verifica = $dao->verificaEmail($obj->email);

    if($verifica > 0){
    	print("duplicado");
        exit();
    }

    if ($obj->ValidaCampos()){
        
        $codigo = $dao->Insert($obj);
    }
    else
        $status = 'aviso';
}
catch (Exception $e){
    $status = 'erro';
}

print($status);
?>

