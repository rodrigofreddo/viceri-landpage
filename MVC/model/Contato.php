<?php

require_once('Uteis.php');

class Contato {

    public $nome;
    public $email;

    public function __construct() {
        $this->nome = "";
		$this->email = "";
    }

    public function ValidaCampos(){
		if (trim($this->nome) == '')
			return(false);

        if (trim($this->email) == '')
            return(false);
        
        return(true);
    }

}
?>
