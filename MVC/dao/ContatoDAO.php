<?php
require_once('conexao.php');

require_once($_SERVER['DOCUMENT_ROOT'] . $pasta . 'MVC/model/Uteis.php');
require_once($_SERVER['DOCUMENT_ROOT'] . $pasta . 'MVC/model/Contato.php');

class ContatoDAO {

	private $sql;
	private $campos = array('nome','email');/* Adicionar todos campos para insert, update */

	public $colunas;
	public $values;
	public $parameters = array();

	public $colunas_update;

    public function __construct(){

	foreach ($this->campos as $campo) {

		$this->colunas .=  $campo . ", ";
		$this->values .= ":" . $campo . ", ";

		$this->colunas_update .= $campo . " = :" . $campo . ", ";

	}

	$this->colunas = substr($this->colunas,0,-2);
	$this->values = substr($this->values,0,-2);
	$this->colunas_update = substr($this->colunas_update,0,-2);

    }

	public function Insert(Contato $obj){

		foreach ($this->campos as $campo) {
			$this->parameters[$campo] = $obj->$campo;
		}

		try {

			$this->sql = "INSERT INTO contato (" . $this->colunas . ") VALUES(" . $this->values . ") ";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute($this->parameters);

			return Conexao::getInstance()->lastInsertId();

		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	

    public function Update(Contato $obj){

		foreach ($this->campos as $campo) {
			$this->parameters[':'.$campo] = $obj->$campo;
		}
		$this->parameters[':codigo'] = $obj->codigo;

		try {

        $this->sql = "UPDATE contato set " . $this->colunas_update . " WHERE codigo = :codigo ";

		$p_sql = Conexao::getInstance()->prepare($this->sql);
		$p_sql->execute($this->parameters);

        }catch(Exception $e){
			return $e->getMessage();
		}
		
    }

    public function Delete($codigo){

		try {

			$this->sql = "DELETE FROM contato WHERE codigo = :codigo";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

        }catch(Exception $e){
			return $e->getMessage();
		}
    }    

    public function getById($codigo){

		try {

			$this->sql = "SELECT * FROM contato
						  WHERE codigo = :codigo ";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

			if ($row = $p_sql->fetch(PDO::FETCH_ASSOC)){
				$obj = new Contato();
				$obj->codigo = $codigo;
				
				foreach ($this->campos as $campo) {
					$obj->$campo = $row[$campo];
				}
			}

		}catch(Exception $e){
			return $e->getMessage();
		}

        return ($obj);
    }

    public function verificaEmail($email){
    	try{

    		$this->sql = "SELECT * FROM contato WHERE email = :email ";
    		$rs = Conexao::getInstance()->prepare($this->sql);
    		$rs->execute(array(':email' => $email));
    		//$row = $rs->fetch(PDO::FETCH_ASSOC);
    		$rows = $rs->rowCount();

    		return $rows;

    	}catch(Exception $e){
			return $e->getMessage();
		}
    }

	

    public function getPesquisa($busca){
		try {
			
			$this->sql = "SELECT * FROM contato
						  
						  WHERE email LIKE :busca 
						  
						  ORDER BY email ASC  ";

			$rs = Conexao::getInstance()->prepare($this->sql);
			$rs->execute(array(':busca' => "%".$busca."%"));
			
			return ($rs);
			
        }catch(Exception $e){
			return $e->getMessage();
		}
    }

    public function getListaAdmin($inicio,$qnt){
		try {


			$this->sql = "SELECT * FROM contato  
						  ORDER BY codigo DESC LIMIT $inicio, $qnt ";	

			$rs = Conexao::getInstance()->prepare($this->sql);
			$rs->execute();

			return ($rs);

        }catch(Exception $e){
			return $e->getMessage();
		}
    }

    public function getListaAlfabetica(){
		try {
			$this->sql = "SELECT * FROM contato  
						  ORDER BY email ASC ";	

			$rs = Conexao::getInstance()->prepare($this->sql);
			$rs->execute();

			return ($rs);

        }catch(Exception $e){
			return $e->getMessage();
		}
    }

   

}
?>
