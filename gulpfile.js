//              _         _       _ _
//   __ _ _   _| |_ __   (_)_ __ (_) |_
//  / _` | | | | | '_ \  | | '_ \| | __|
// | (_| | |_| | | |_) | | | | | | | |_
//  \__, |\__,_|_| .__/  |_|_| |_|_|\__|
//  |___/        |_|

var gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	tinypng = require('gulp-tinypng'),
	fs = require('fs'),
	del = require('del'),
	runSequence = require('run-sequence'),
	plugins = require('gulp-load-plugins')({
		rename: {
			'gulp-util': 'gutil',
			'gulp-compass': 'compass',
    		'gulp-minify-css': 'minifyCss',
    		'gulp-sourcemaps': 'sourceMaps',
    		'gulp-jshint': 'jshint'
  		}
  	});


//                  _       _     _
// __   ____ _ _ __(_) __ _| |__ | | ___  ___
// \ \ / / _` | '__| |/ _` | '_ \| |/ _ \/ __|
//  \ V / (_| | |  | | (_| | |_) | |  __/\__ \
//   \_/ \__,_|_|  |_|\__,_|_.__/|_|\___||___/

var src = __dirname+'/src',
	dest = __dirname+'/assets',
	lib = __dirname+'/lib',
	pkg = JSON.parse(fs.readFileSync('package.json')),
	version = parseInt(pkg.version),
	bannerFiles = '/**\n' +
                  ' * Version '+ version + '\n' +
                  ' * @date <%= new Date() %>\n' +
                  ' */\n\n';


//                             _                     _ _
//   ___ _ __ _ __ ___  _ __  | |__   __ _ _ __   __| | | ___ _ __
//  / _ \ '__| '__/ _ \| '__| | '_ \ / _` | '_ \ / _` | |/ _ \ '__|
// |  __/ |  | | | (_) | |    | | | | (_| | | | | (_| | |  __/ |
//  \___|_|  |_|  \___/|_|    |_| |_|\__,_|_| |_|\__,_|_|\___|_|

var onError = function (err) {
    plugins.gutil.log('|- ' + plugins.gutil.colors.bgRed.bold('Build Error in ' + err.plugin));
    plugins.gutil.log('|- ' + plugins.gutil.colors.bgRed(err.message));

	plugins.gutil.beep();
    this.emit('end');
}


//                 _            _
//   ___ ___ ___  | |_ __ _ ___| | __
//  / __/ __/ __| | __/ _` / __| |/ /
// | (__\__ \__ \ | || (_| \__ \   <
//  \___|___/___/  \__\__,_|___/_|\_\

gulp.task('compile-css',function() {
	return gulp.src(['src/scss/**/*.scss'])
		.pipe(plugins.plumber({
			errorHandler: onError
		}))
		.pipe(plugins.compass({
			css: dest+'/css/',
			sass: src+'/scss/',
			image: src+'/img/',
			generated_images_path: dest+'/img/',
			relative_assets: true,
			sourcemap: true
		}))
		.pipe(plugins.minifyCss({ compatibility: 'ie9' }))
		.pipe(plugins.rename({ suffix: '.min' }))
		.pipe(plugins.header(bannerFiles))
		.pipe(gulp.dest('assets/css'))
});


//    _       _            _
//   (_)___  | |_ __ _ ___| | __
//   | / __| | __/ _` / __| |/ /
//   | \__ \ | || (_| \__ \   <
//  _/ |___/  \__\__,_|___/_|\_\
// |__/

gulp.task('compile-js',function() {
	for (var i = 0; i <= version; i++) {
		var vendor = JSON.parse(fs.readFileSync('vendor.json')),
			files = vendor.versions[i];

		if (!files) {
			files = [];
		}

		gulp.src(files)
			.pipe(plugins.plumber({
		        errorHandler: onError
		    }))
			// .pipe(plugins.sourceMaps.init())
			.pipe(plugins.concat('vendor.js'))
			.pipe(plugins.uglify())
			.pipe(plugins.header(bannerFiles))
			.pipe(plugins.rename({ suffix: '.min' }))
			// .pipe(plugins.sourceMaps.write())
			.pipe(gulp.dest(dest+'/js'))
	}

	gulp.src([src+'/js/*.js'])
		.pipe(plugins.jshint())
		.pipe(plugins.jshint.reporter('jshint-stylish'))
		.pipe(plugins.plumber({
	        errorHandler: onError
	    }))
		// .pipe(plugins.sourceMaps.init())
		.pipe(plugins.concat('app.js'))
		.pipe(plugins.uglify())
		.pipe(plugins.header(bannerFiles))
		.pipe(plugins.rename({ suffix: '.min' }))
		// .pipe(plugins.sourceMaps.write())
		.pipe(gulp.dest(dest+'/js'))

	return;
});


//  _
// (_)_ __ ___   __ _
// | | '_ ` _ \ / _` |
// | | | | | | | (_| |
// |_|_| |_| |_|\__, |
//              |___/

gulp.task('compile-img', function () {
  return gulp.src([src+'/img/**/*.{png,jpg,gif,ico}', '!'+src+'/img/sprite', '!'+src+'/img/sprite/*.png'])
    .pipe(gulp.dest(dest+'/img'))
});

gulp.task('tinypng', function () {
    gulp.src([dest+'/img/**/*.{png,jpg}', '!'+dest+'/img/sprite-*.png'])
        .pipe(tinypng('d2v79DcN9HloZ7dZiZGu2ZpVwak9HzfT'))
        .pipe(gulp.dest(dest+'/img'));
});


//  _                             _            _
// | |__  _   _ _ __ ___  _ __   | |_ __ _ ___| | __
// | '_ \| | | | '_ ` _ \| '_ \  | __/ _` / __| |/ /
// | |_) | |_| | | | | | | |_) | | || (_| \__ \   <
// |_.__/ \__,_|_| |_| |_| .__/   \__\__,_|___/_|\_\
//                       |_|

gulp.task('bump', function () {
    var type = plugins.argv.type,
    	version = plugins.argv.version,
    	options = {};

    if (version) {
        options.version = version;
        msg += ' to ' + version;
    } else {
        options.type = type;
        msg += ' for a ' + type;
    }

    return gulp
        .src('package.json')
        .pipe(plugins.bump(options))
        .pipe(gulp.dest('/'));
});


//       _
//   ___| | ___  __ _ _ __     ___ ___ ___
//  / __| |/ _ \/ _` | '_ \   / __/ __/ __|
// | (__| |  __/ (_| | | | | | (__\__ \__ \
//  \___|_|\___|\__,_|_| |_|  \___|___/___/

gulp.task('clean-css', function() {
	return del([dest+'/css/main.css', dest+'/css/main.css.map']);
});


//       _                    _
//   ___| | ___  __ _ _ __   (_)_ __ ___   __ _
//  / __| |/ _ \/ _` | '_ \  | | '_ ` _ \ / _` |
// | (__| |  __/ (_| | | | | | | | | | | | (_| |
//  \___|_|\___|\__,_|_| |_| |_|_| |_| |_|\__, |
//                                        |___/

gulp.task('clean-img', function() {
	return del(dest+'/img');
});

//      _            _
//   __| | ___ _ __ | | ___  _   _
//  / _` |/ _ \ '_ \| |/ _ \| | | |
// | (_| |  __/ |_) | | (_) | |_| |
//  \__,_|\___| .__/|_|\___/ \__, |
//            |_|            |___/

gulp.task('deploy', function() {
	return runSequence('clean-img', 'compile-css', 'clean-css', 'compile-js', 'compile-img', 'tinypng');
});


//                _       _       _            _
// __      ____ _| |_ ___| |__   | |_ __ _ ___| | __
// \ \ /\ / / _` | __/ __| '_ \  | __/ _` / __| |/ /
//  \ V  V / (_| | || (__| | | | | || (_| \__ \   <
//   \_/\_/ \__,_|\__\___|_| |_|  \__\__,_|___/_|\_\

gulp.task('watch', function() {
	// TODO:
	// there's an error that files moved to sprite folder doesn't run compile-css as expected
	gulp.watch([src+'/scss/**/*.scss', src+'/img/sprite/*.png'], ['compile-css']);
	gulp.watch([src+'/js/**/*.js', 'vendor.json'], ['compile-js']);
	gulp.watch(src+'/img/*.{jpg,png,gif,ico}', ['compile-img']);
});


//      _       __             _ _     _            _
//   __| | ___ / _| __ _ _   _| | |_  | |_ __ _ ___| | __
//  / _` |/ _ \ |_ / _` | | | | | __| | __/ _` / __| |/ /
// | (_| |  __/  _| (_| | |_| | | |_  | || (_| \__ \   <
//  \__,_|\___|_|  \__,_|\__,_|_|\__|  \__\__,_|___/_|\_\

gulp.task('default', ['compile-css', 'compile-js', 'compile-img', 'watch']);
