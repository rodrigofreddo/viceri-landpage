<!doctype html>
<html lang="pt-br">
	
	<head>
		<title>Laguetto Stilo Itapema</title>
		
		<meta name="description" content="O Laghetto Stilo Itapema &eacute; o primeiro hotel da Rede Laghetto em Santa Catarina." />
		<?php include('includes/head.php') ?>
		
	</head>

	<body>

		<section id=header>
			<div class="container">
				<div class="row justify-content-center margin-mobile">
					<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-4">
						<a title="Viceri" target="_blank" href="https://www.viceri.com.br/"><img class="logo-header" alt="Viceri" src="{{ URL::to('assets/front/img/includes/logo-header.svg') }}"></a>
					</div>

					<div class="col-md-2 col-sm-3 d-sm-block d-none">
						<p>Propulsione a<br>
						revolu��o digital.</p>
					</div>

					<div class="col-md-6 col-sm-4 col-8 text-right">
						<nav class="social-medias text-right">
							<ul>
								<li>
									<a target="_blank" title="Facebook" href="https://www.facebook.com/GViceri"><svg data-name="001-facebook" xmlns="http://www.w3.org/2000/svg"><path data-name="Caminho 6242" d="M13.934 23.844q-.15.024-.3.045.151-.021.3-.045zm0 0"/><path data-name="Caminho 6243" d="M14.175 23.803l-.143.025zm0 0"/><path data-name="Caminho 6244" d="M13.366 23.922q-.176.02-.353.035.178-.015.353-.035zm0 0"/><path data-name="Caminho 6245" d="M13.597 23.894l-.169.021zm0 0"/><path data-name="Caminho 6246" d="M14.502 23.738l-.127.027zm0 0"/><path data-name="Caminho 6247" d="M15.313 23.536l-.1.028zm0 0"/><path data-name="Caminho 6248" d="M15.067 23.604l-.111.029zm0 0"/><path data-name="Caminho 6249" d="M14.747 23.684l-.118.027zm0 0"/><path data-name="Caminho 6250" d="M13.009 23.957l-.19.014zm0 0"/><path data-name="Caminho 6251" d="M24 12a12 12 0 1 0-12 12h.211v-9.344H9.633v-3h2.578V9.44a3.609 3.609 0 0 1 3.853-3.96 21.223 21.223 0 0 1 2.311.118v2.68H16.8c-1.244 0-1.485.591-1.485 1.459v1.914h2.975l-.388 3h-2.59v8.881A12.006 12.006 0 0 0 24 12zm0 0"/><path data-name="Caminho 6252" d="M12.793 23.974q-.187.012-.376.019.188-.007.376-.019zm0 0"/><path data-name="Caminho 6253" d="M12.407 23.992h-.2zm0 0"/></svg></a>
								</li>
								<li>
									<a target="_blank" title="Instagram" href="https://www.instagram.com/_viceri/"><svg data-name="011-instagram" xmlns="http://www.w3.org/2000/svg"><path data-name="Caminho 6254" d="M14.297 12.003a2.3 2.3 0 1 1-2.3-2.3 2.3 2.3 0 0 1 2.3 2.3zm0 0"/><path data-name="Caminho 6255" d="M17.371 7.936a2.282 2.282 0 0 0-1.308-1.308 3.812 3.812 0 0 0-1.279-.237c-.727-.033-.945-.04-2.784-.04s-2.058.007-2.784.04a3.815 3.815 0 0 0-1.279.237 2.283 2.283 0 0 0-1.308 1.308 3.814 3.814 0 0 0-.237 1.28c-.033.727-.04.944-.04 2.784s.007 2.058.04 2.784a3.812 3.812 0 0 0 .237 1.279 2.282 2.282 0 0 0 1.308 1.308 3.808 3.808 0 0 0 1.28.237c.727.033.944.04 2.784.04s2.058-.007 2.784-.04a3.808 3.808 0 0 0 1.28-.237 2.282 2.282 0 0 0 1.308-1.308 3.816 3.816 0 0 0 .237-1.279c.033-.727.04-.945.04-2.784s-.007-2.058-.04-2.784a3.808 3.808 0 0 0-.239-1.28zm-5.372 7.6a3.538 3.538 0 1 1 3.538-3.538A3.538 3.538 0 0 1 12 15.539zm3.678-6.39a.827.827 0 1 1 .827-.827.827.827 0 0 1-.826.83zm0 0"/><path data-name="Caminho 6256" d="M12 0a12 12 0 1 0 12 12A12 12 0 0 0 12 0zm6.849 14.841a5.055 5.055 0 0 1-.32 1.673 3.523 3.523 0 0 1-2.015 2.015 5.059 5.059 0 0 1-1.672.32c-.735.034-.97.042-2.841.042s-2.106-.008-2.841-.042a5.059 5.059 0 0 1-1.672-.32 3.523 3.523 0 0 1-2.015-2.015 5.054 5.054 0 0 1-.32-1.672c-.034-.735-.042-.97-.042-2.841s.008-2.106.042-2.841a5.056 5.056 0 0 1 .32-1.673 3.526 3.526 0 0 1 2.013-2.016 5.06 5.06 0 0 1 1.673-.32c.735-.034.97-.042 2.841-.042s2.106.008 2.841.042a5.061 5.061 0 0 1 1.673.32 3.524 3.524 0 0 1 2.015 2.015 5.055 5.055 0 0 1 .32 1.673c.034.735.041.97.041 2.841s-.007 2.106-.041 2.841zm0 0"/></svg></a>
								</li>
								<li>
									<a target="_blank" title="Linkedin" href="https://www.linkedin.com/company/grupo-viceri/"><svg xmlns="http://www.w3.org/2000/svg"><path data-name="010-linkedin" d="M12 0a12 12 0 1 0 12 12A12 12 0 0 0 12 0zM8.513 18.141H5.59V9.348h2.923zM7.052 8.147h-.019a1.523 1.523 0 1 1 .038-3.038 1.524 1.524 0 1 1-.019 3.038zm12 9.993h-2.923v-4.7c0-1.182-.423-1.988-1.481-1.988a1.6 1.6 0 0 0-1.5 1.069 2 2 0 0 0-.1.713v4.91H10.13s.038-7.968 0-8.793h2.922v1.245a2.9 2.9 0 0 1 2.634-1.451c1.923 0 3.365 1.257 3.365 3.957zm0 0"/></svg></a>
								</li>
								<li>
									<a target="_blank" title="Youtube" href="https://www.youtube.com/channel/UCT7ASfVzopA0Rtsn0hm2RsA/featured"><svg data-name="008-youtube" xmlns="http://www.w3.org/2000/svg"><path data-name="Caminho 6257" d="M10.505 14.248l3.9-2.248-3.9-2.248zm0 0"/><path data-name="Caminho 6258" d="M12 0a12 12 0 1 0 12 12A12 12 0 0 0 12 0zm7.5 12.012a19.806 19.806 0 0 1-.309 3.607 1.879 1.879 0 0 1-1.322 1.322A45.609 45.609 0 0 1 12 17.25a43.977 43.977 0 0 1-5.868-.321 1.879 1.879 0 0 1-1.322-1.322A19.719 19.719 0 0 1 4.5 12a19.793 19.793 0 0 1 .309-3.607 1.917 1.917 0 0 1 1.323-1.334A45.609 45.609 0 0 1 12 6.75a43.884 43.884 0 0 1 5.868.321 1.879 1.879 0 0 1 1.322 1.322 18.8 18.8 0 0 1 .31 3.619zm0 0"/></svg></a>
								</li>
							</ul>
						</nav>

						<div class="menu-burguer d-none">
							<svg xmlns="http://www.w3.org/2000/svg" width="28.5" height="24"><g data-name="Grupo 26"><path data-name="Caminho 43788" d="M1.652 3.37h25.2a1.685 1.685 0 0 0 0-3.369h-25.2a1.685 1.685 0 0 0 0 3.369zm25.2 6.947h-25.2a1.685 1.685 0 0 0 0 3.369h25.2a1.685 1.685 0 0 0 0-3.369zm0 10.315h-25.2a1.685 1.685 0 0 0 0 3.369h25.2a1.685 1.685 0 0 0 0-3.369z"/></g></svg>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="page-carreiras">
			<section id="banner-1">
				<div class="mask"></div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-4 col-md-5">
							<h1 class="title-page">Carreiras em TI </h1>
						</div>
						<div class="col-lg-6 col-md-6 text-md-right text-center">
							<p class="text-left">Fazer a Revolu��o Digital precisa de pessoas com pensamento disruptivo e agilidade. Voc� pode ser uma dessas pessoas!</p>
						</div>
					</div>
				</div>
			</section>

			<section id="certificado">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-5 col-sm-4 text-center">
							<img title="Certificado GPTW" alt="Certificado GPTW" src="{{ URL::to('assets/front/img/carreiras/selo-gptw.png')}}">
						</div>
						<div class="col-lg-1 d-lg-block d-none"></div>
						<div class="col-lg-4 col-md-5 col-sm-7">
							<h2 class="title-banner text-left">Somos uma excelente empresa para se trabalhar certificada pela Great Place to Work</h2>
							<p class="text-banner">Respeito pelos nossos colaboradores � uma das marcas da nossa empresa. Temos uma pontua��o geral de 86 na avalia��o da Great Place to Work. � assim que constru�mos funcion�rios comprometidos com cada novo projeto.</p>
						</div>
					</div>
				</div>
			</section>

			<section id="banner-2">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-4 col-md-4 col-sm-11">
							<h2 class="title-banner">Construa sua carreira<br> com a Viceri e venha<br> fazer a transforma��o<br> digital acontecer.</h2>
							<p class="text-banner">Nosso objetivo � construir os neg�cios do futuro e nossos colaboradores fazem parte disso. Por isso, investimos na qualifica��o com treinamentos na Universidade Corporativa e proporcionamos qualidade de vida no trabalho atrav�s de um ambiente saud�vel que favorece a inspira��o e a inova��o.</p>
						</div>
						<div class="col-lg-6 col-md-7 col-sm-11"></div>				
					</div>
				</div>
			</section>

			<section id="vagas">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-10 col-md-11 col-sm-11">
							<h2 class="title-section">Vagas</h2>
						</div>
					</div>

					<div class="row justify-content-center">
						<div class="col-lg-3 col-md-3 col-sm-5 text-sm-left text-center">
							<div class="box">
								<h3>Analista Programador VB6</h3>
								<p>Jundia� - S�o Paulo</p>
								<div class="text-center">
									<button onclick="scrollToTarget('#curriculo');" class="btn">Candidate-se!</button>
								</div>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-5 text-md-center text-sm-right text-center">
							<div class="pd-column">
								<div class="box text-sm-left text-center">
									<h3>Analista Programador ASP</h3>
									<p>Jundia� - S�o Paulo</p>
									<div class="text-center">
										<button onclick="scrollToTarget('#curriculo');" class="btn">Candidate-se!</button>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-sm-5 text-md-right text-center">
							<div class="box last text-sm-left text-center">
								<h3>Analista Programador .NET</h3>
								<p>Jundia� - S�o Paulo</p>
								<div class="text-center">
									<button onclick="scrollToTarget('#curriculo');" class="btn">Candidate-se!</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="vantagens">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-10 col-md-11 col-sm-11">
							<h2 class="title-section">Vantagens</h2>
						</div>
					</div>

					<div class="row justify-content-center">
						<div class="col-lg-2 col-md-3 col-sm-3">
							<div class="box">
								<img src="{{ URL::to('assets/front/img/carreiras/desenvolvimento.svg')}}">
								
								<h3>Desenvolvimento profissional</h3>
								
								<p>Autonomia para inovar e desenvolver as melhores solu��es digitais;</p>
								<p>Feedbacks constantes para evolu��o profissional da sua carreira em TI;</p>
								<p>Treinamentos para aperfei�oamento de habilidades na Universidade Corporativa.</p>	
							</div>
						</div>

						<div class="col-lg-2 col-md-1 col-sm-1"></div>

						<div class="col-lg-2 col-md-3 col-sm-3">
							<div class="box">
								<img class="small-img" src="{{ URL::to('assets/front/img/carreiras/qualidade.svg')}}">
								
								<h3 class="no-padding-mobile">Qualidade de vida no trabalho </h3>
								
								<p>Hor�rios flex�veis;</p>
								<p>Ambiente descontra�do para favorecer a inspira��o e criatividade;</p>
								<p>Empregos em Jundia�, S�o Paulo, uma das melhores cidades para se viver segundo o �ndice Firjan de Desenvolvimento Municipal.</p>	
							</div>
						</div>

						<div class="col-lg-2 col-md-1 col-sm-1"></div>

						<div class="col-lg-2 col-md-3 col-sm-3">
							<div class="box">
								<img src="{{ URL::to('assets/front/img/carreiras/reconhecimento.svg')}}">
								
								<h3>Reconhecimento</h3>
								
								<p>Premia��es para entregas com resultados superiores;</p>
								<p>Evolu��o da sua carreira baseada em Drivers de Crescimento;</p>
								<p>Reconhecimento para lideran�a e inova��o nas solu��es dos nossos clientes.</p>	
							</div>
						</div>
					</div>
				</div>
			</section>		

			<section id="valores">
				<div class="mask"></div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-10 col-md-11 col-sm-11">
							<h2 class="title-section text-left">Valores</h2>
						</div>
					</div>

					<div class="row justify-content-center">
						<div class="col-lg-5 col-md-5 col-sm-5">
							<div class="box toLeft">
								<p>Somos obcecados pelo cliente e estamos focados em <span>entregar solu��es impactantes</span> e que gerem valor ao neg�cio.</p>

								<p><span>Mindset de crescimento aberto ao novo</span>. As solu��es nunca imaginadas podem ser a chave para a transforma��o do cliente.</p>

								<p>Temos esp�rito de <span>lideran�a e autonomia</span> para tomar decis�es r�pidas e responsabilidade para assumir riscos.</p>
							</div>
						</div>

						<div class="d-lg-none d-block col-md-1 col-sm-1"></div>

						<div class="col-lg-5 col-md-5 col-md-5 col-sm-5">
							<div class="box toRight">
								<p>Respondemos a complexidade e incertezas de maneira r�pida atrav�s de <span>metodologias �geis e solu��es simples</span>.</p>

								{{--<p><span>Trabalho em equipe e confian�a</span> para desenvolver os neg�cios dos clientes e tamb�m as nossas carreiras.</p>--}}
								<p><span>Trabalho em equipe e esp�rito de fam�lia</span> para desenvolver os neg�cios dos clientes e tamb�m as nossas carreiras.</p>

								<p>Estamos em <span>crescimento e aprendizado</span> constante para desenvolver a criatividade e a inova��o necess�rios para transforma��o digital dos nossos clientes.</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="newsletter">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-4 col-md-5 col-sm-6">
							<h2>Fique por dentro de tudo que acontece na Viceri</h2>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-5 text-sm-right text-center">
							<a title="Come�ar" target="_blank" href="https://www.viceri.com.br/blog/"><button class="btn">Come�ar</button></a>
						</div>
					</div>
				</div>
			</section>

			<section id="curriculo">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-10 col-md-11 col-sm-11">
							<h2 class="title-section">Venha fazer parte da nossa equipe</h2>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-xl-4 col-lg-5 col-md-6 col-sm-11 text-sm-left text-center">
							<form id="curriculo-form" method="post" action="{{URL::to('/contato')}}">
								{{ csrf_field() }}
								<input type="text" name="Ename" id="Ename" placeholder="Nome" class="required" />
								<input type="text" name="Eemail" id="Eemail" class="required email" placeholder="Email"/>
								<input type="text" name="Ephone" id="Ephone" class="required mask-input phone" maxlength="15" placeholder="Telefone"/>
								<input type="text" name="Earea" id="Earea" class="required margin" placeholder="�rea de atua��o"/>
								<input type="file" name="file" id="file" class="required d-none">
							</form>
							<div class="add-file" onclick="$(file).trigger('click')">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24.002"><g fill="#00ae9b"><path data-name="Caminho 43795" d="M22.343 1.66a5.684 5.684 0 0 0-8.027 0l-4.014 4.014a.873.873 0 0 0 1.235 1.235l4.014-4.014a3.93 3.93 0 0 1 5.557 5.558L15.86 13.7a3.936 3.936 0 0 1-5.558 0 .873.873 0 0 0-1.235 1.235 5.683 5.683 0 0 0 8.027 0l5.249-5.249a5.69 5.69 0 0 0 0-8.028z"/><path data-name="Caminho 43796" d="M11.846 17.715l-3.4 3.4a3.93 3.93 0 1 1-5.558-5.558l4.94-4.94a3.944 3.944 0 0 1 5.557 0 .873.873 0 0 0 1.235-1.235 5.683 5.683 0 0 0-8.027 0l-4.936 4.937a5.676 5.676 0 0 0 8.027 8.023l3.4-3.4a.873.873 0 0 0-1.234-1.235z"/></g></svg>
								<p>Anexar arquivo</p>
							</div>
							<button type="button" class="btn" id="btn-curriculo" data-form="curriculo-form" onclick="APP.enviaForm(event)">Enviar Curr�culo</button>
						</div>

						<div class="col-xl-2 col-lg-1 col-md-1"></div>
						<div class="col-lg-4 col-md-4">
							<h3 class="text-curriculo">Venha para a<br>
							Viceri propulsionar<br>
							a Revolu��o Digital<br>
							dos neg�cios</h3>
						</div>
					</div>
				</div>
			</section>
		</div>

		<script type="text/javascript">
				function scrollToTarget($target) {
					$('html,body').animate({
					scrollTop: $($target).offset().top},'slow');
				}
		</script>

		<section id="footer">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-2 col-md-2 d-none">
						<nav>
							<ul>
								<li><a href="">Solu��es</a></li>
								<li><a href="">Clientes</a></li>
								<li><a href="">Materiais</a></li>
								<li><a href="">A Viceri</a></li>
							</ul>
						</nav>
					</div>

					<div class="col-lg-2 col-md-2 d-none">
						<nav>
							<ul>
								<li><a href="">Carreiras</a></li>
								<li><a href="">Blog</a></li>
								<li><a href="">Parceiros</a></li>
								<li><a href="">Contato</a></li>
							</ul>
						</nav>
					</div>

					{{--<div class="col-lg-6 col-md-6 col-sm-6 text-right">--}}
					{{--Descomentar quando deixar� de ser s� uma landpage--}}
					<div class="col-lg-10 col-md-11 col-sm-11 text-center">
						<nav class="social-medias text-center">
							<ul>
								<li>
									<a target="_blank" title="Facebook" href="https://www.facebook.com/GViceri"><svg data-name="001-facebook" xmlns="http://www.w3.org/2000/svg" width="32" height="32"><path data-name="Caminho 6242" d="M18.58 31.792q-.2.033-.4.06.2-.028.4-.06zm0 0" /><path data-name="Caminho 6243" d="M18.9 31.737l-.191.033zm0 0" /><path data-name="Caminho 6244" d="M17.821 31.896q-.234.026-.47.046.236-.02.47-.046zm0 0" /><path data-name="Caminho 6245" d="M18.129 31.858l-.226.028zm0 0" /><path data-name="Caminho 6246" d="M19.335 31.65l-.169.035zm0 0" /><path data-name="Caminho 6247" d="M20.417 31.382l-.136.038zm0 0" /><path data-name="Caminho 6248" d="M20.09 31.471l-.148.039zm0 0" /><path data-name="Caminho 6249" d="M19.662 31.578l-.158.035zm0 0" /><path data-name="Caminho 6250" d="M17.345 31.943l-.253.019zm0 0" /><path data-name="Caminho 6251" d="M32 16a16 16 0 1 0-16 16h.281V19.541h-3.437v-4.006h3.438v-2.948c0-3.419 2.087-5.28 5.137-5.28a28.3 28.3 0 0 1 3.082.157v3.573H22.4c-1.659 0-1.98.789-1.98 1.946v2.552h3.967l-.517 4.006h-3.45v11.841A16.009 16.009 0 0 0 32 16zm0 0" /><path data-name="Caminho 6252" d="M17.057 31.965q-.25.016-.5.025.25-.009.5-.025zm0 0" /><path data-name="Caminho 6253" d="M16.548 31.99l-.266.007zm0 0" /></svg></a>
								</li>
								<li>
									<a target="_blank" title="Instagram" href="https://www.instagram.com/_viceri/"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"><g data-name="011-instagram"><path data-name="Caminho 6254" d="M19.062 16A3.063 3.063 0 1 1 16 12.937 3.062 3.062 0 0 1 19.062 16zm0 0"/><path data-name="Caminho 6255" d="M23.162 10.58a3.042 3.042 0 0 0-1.744-1.744 5.083 5.083 0 0 0-1.706-.316c-.969-.044-1.26-.054-3.713-.054s-2.744.009-3.713.053a5.086 5.086 0 0 0-1.706.316 3.044 3.044 0 0 0-1.744 1.744 5.085 5.085 0 0 0-.316 1.706c-.044.969-.054 1.259-.054 3.713s.01 2.744.054 3.713a5.083 5.083 0 0 0 .316 1.706 3.042 3.042 0 0 0 1.744 1.744 5.077 5.077 0 0 0 1.706.316c.969.044 1.259.053 3.712.053s2.744-.009 3.713-.053a5.077 5.077 0 0 0 1.706-.316 3.042 3.042 0 0 0 1.744-1.744 5.088 5.088 0 0 0 .316-1.706c.044-.969.053-1.26.053-3.713s-.009-2.744-.053-3.713a5.077 5.077 0 0 0-.315-1.705zM16 20.716a4.718 4.718 0 1 1 4.718-4.718A4.718 4.718 0 0 1 16 20.718zm4.9-8.52a1.1 1.1 0 1 1 1.1-1.1 1.1 1.1 0 0 1-1.096 1.103zm0 0"/><path data-name="Caminho 6256" d="M16 0a16 16 0 1 0 16 16A16 16 0 0 0 16 0zm9.132 19.788a6.741 6.741 0 0 1-.427 2.23 4.7 4.7 0 0 1-2.687 2.687 6.745 6.745 0 0 1-2.23.427c-.98.045-1.293.055-3.788.055s-2.808-.011-3.788-.055a6.745 6.745 0 0 1-2.23-.427A4.7 4.7 0 0 1 7.3 22.018a6.739 6.739 0 0 1-.427-2.23c-.045-.98-.056-1.293-.056-3.788s.01-2.808.055-3.788a6.741 6.741 0 0 1 .427-2.23 4.7 4.7 0 0 1 2.683-2.687 6.747 6.747 0 0 1 2.23-.427c.98-.045 1.293-.055 3.788-.055s2.808.011 3.788.056a6.748 6.748 0 0 1 2.23.427 4.7 4.7 0 0 1 2.687 2.687 6.74 6.74 0 0 1 .427 2.23c.045.98.055 1.293.055 3.788s-.01 2.807-.055 3.787zm0 0"/></g></svg></a>
								</li>
								<li>
									<a target="_blank" title="Linkedin" href="https://www.linkedin.com/company/grupo-viceri/"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"><path data-name="010-linkedin" d="M16 0a16 16 0 1 0 16 16A16 16 0 0 0 16 0zm-4.649 24.188h-3.9V12.464h3.9zM9.4 10.863h-.023a2.031 2.031 0 1 1 .051-4.051 2.032 2.032 0 1 1-.028 4.051zm16 13.324h-3.9v-6.271c0-1.576-.564-2.651-1.974-2.651a2.133 2.133 0 0 0-2 1.425 2.668 2.668 0 0 0-.128.951v6.547h-3.9s.051-10.624 0-11.723h3.9v1.66a3.869 3.869 0 0 1 3.512-1.935c2.564 0 4.486 1.676 4.486 5.276zm0 0"/></svg></a>
								</li>
								<li>
									<a target="_blank" title="Youtube" href="https://www.youtube.com/channel/UCT7ASfVzopA0Rtsn0hm2RsA/featured"><svg data-name="008-youtube" xmlns="http://www.w3.org/2000/svg" width="32" height="32"><path data-name="Caminho 6257" d="M14.007 18.997l5.2-3-5.2-3zm0 0" /><path data-name="Caminho 6258" d="M16 0a16 16 0 1 0 16 16A16 16 0 0 0 16 0zm10 16.016a26.408 26.408 0 0 1-.412 4.81 2.505 2.505 0 0 1-1.762 1.762C22.259 23 16 23 16 23s-6.242 0-7.823-.428a2.506 2.506 0 0 1-1.762-1.762A26.292 26.292 0 0 1 6 16a26.39 26.39 0 0 1 .412-4.809 2.556 2.556 0 0 1 1.765-1.779C9.741 9 16 9 16 9s6.259 0 7.823.428a2.506 2.506 0 0 1 1.762 1.762A25.06 25.06 0 0 1 26 16.016zm0 0" /></svg></a>
								</li>
							</ul>
						</nav>
						<p class="text-footer">Propulsione a<br> revolu��o digital.</p>
					</div>
				</div>

				<div class="row justify-content-center margin">
					{{--<div class="col-xl-8 col-lg-7 col-md-8 col-sm-8 text-sm-left text-center">--}}
					<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 text-sm-left text-center">
						<p class="copyright">Viceri � 2019 Todos os direitos reservado</p>	
					</div>
					{{--<div class="col-xl-2 col-lg-3 col-md-3 col-sm-3 text-sm-left text-center">--}}
						{{--<div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 text-sm-left text-center">--}}
						<a title="Viceri" target="_blank" href="https://www.viceri.com.br/"><img class="logo-footer" alt="Viceri" src="{{ URL::to('assets/front/img/includes/logo-footer.svg') }}"></a>
					</div>
				</div>
			</div>
		</section>

<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-77098167-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-77098167-1');
</script>-->

	</body>

</html>